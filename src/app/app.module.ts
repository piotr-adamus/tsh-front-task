import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {SuppliersContainerComponent} from './suppliers-container/suppliers-container.component';
import {SearchBarComponent} from './suppliers-container/search-bar/search-bar.component';
import {SupplierComponent} from './suppliers-container/suppliers-list/supplier/supplier.component';
import {SuppliersListComponent} from './suppliers-container/suppliers-list/suppliers-list.component';
import {SharedModule} from './suppliers-container/shared/shared.module';
import {HttpClientModule} from '@angular/common/http';
import { SuppliersPaginationComponent } from './suppliers-container/suppliers-pagination/suppliers-pagination.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SupplierModalContentComponent} from './suppliers-container/supplier-modal-content/supplier-modal-content.component';
import { SuppliersPaginationDirective } from './suppliers-container/suppliers-pagination/directives/suppliers-pagination.directive';


@NgModule({
  declarations: [
    AppComponent,
    SuppliersContainerComponent,
    SearchBarComponent,
    SuppliersContainerComponent,
    SupplierComponent,
    SuppliersListComponent,
    SuppliersPaginationComponent,
    SupplierModalContentComponent,
    SuppliersPaginationDirective
  ],
  // just for testing purposes
  exports: [
    AppComponent,
    SuppliersContainerComponent,
    SearchBarComponent,
    SuppliersContainerComponent,
    SupplierComponent,
    SuppliersListComponent,
    SuppliersPaginationComponent,
    SupplierModalContentComponent,
    SuppliersPaginationDirective
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    SharedModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
