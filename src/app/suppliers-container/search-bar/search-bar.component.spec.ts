import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {SearchBarComponent} from './search-bar.component';
import {By} from '@angular/platform-browser';
import {ReactiveFormsModule} from '@angular/forms';

describe('SearchBarComponent', () => {
  let component: SearchBarComponent;
  let fixture: ComponentFixture<SearchBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchBarComponent],
      imports: [ReactiveFormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should create form', () => {
    expect(component.searchBarForm).toBeTruthy();
    expect(component.searchBarForm.get('query')).not.toBe(undefined);
    expect(component.searchBarForm.get('rating')).toBeTruthy();
  });

  it('shouldEmitSearchButtonClicked', () => {
    component.searchBarForm.setValue({query: 'audit', rating: 4});
    const searchButton = fixture.debugElement.query(By.css('button.btn.btn-search'));
    component.onSearchClicked.subscribe((ev) => {
      expect(ev).toEqual({query: 'audit', rating: 4});
    });
    searchButton.triggerEventHandler('click', null);
  });

  it('shouldEmitResetButtonClicked', () => {
    component.onSearchClicked.subscribe((ev) => {
      console.log(ev);
      expect(ev).toEqual({query: '', rating: ''});
    });
    const resetButton = fixture.debugElement.query(By.css('button.btn.btn-reset'));
    resetButton.triggerEventHandler('click', null);
  });
});
