import {Component, EventEmitter, Output} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";


@Component({
  selector: 'tsh-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent {

  @Output() onSearchClicked = new EventEmitter<{ rating: string, query: string }>();
  searchBarForm: FormGroup;

  constructor(private fb: FormBuilder ) {
    this.createForm();
  }


  createForm() {
    this.searchBarForm = this.fb.group({
      query: '',
      rating: '',
    });
  }

  searchWithParams(): void {
    this.onSearchClicked.emit(this.searchBarForm.value);
  }

  resetFilters(): void {
    this.onSearchClicked.emit({rating: '', query: ''});
    this.searchBarForm.reset({query: '', rating: ''});
  }

}
