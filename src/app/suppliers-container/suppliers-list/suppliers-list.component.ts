import {Component, Input, ViewChild} from '@angular/core';
import {Payment} from '../shared/models/payment';
import {ModalComponent} from '../shared/modal-component/modal.component';

@Component({
  selector: 'tsh-supplier-list',
  templateUrl: './suppliers-list.component.html',
  styleUrls: ['./suppliers-list.component.scss']
})
export class SuppliersListComponent {

  @Input() suppliers: Array<Payment> = null;
  @ViewChild(ModalComponent) modal: ModalComponent;

  supplier: Payment = null;

  constructor() {
  }


  paymentSelected(payment: Payment): void {
    this.supplier = payment;
    if (this.modal != null) {
      this.modal.open = true;
    }
  }
}
