import {Component, Input} from '@angular/core';
import {Payment} from '../../shared/models/payment';

@Component({
  selector: 'tsh-supplier',
  templateUrl: './supplier.component.html',
  styleUrls: ['./supplier.component.scss']
})
export class SupplierComponent {

  @Input() supplier: Payment = null;
  @Input() odd = false;
  @Input() poundRating: number = null;

  constructor() {
  }

}
