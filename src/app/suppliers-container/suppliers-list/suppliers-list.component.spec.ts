import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SuppliersListComponent} from './suppliers-list.component';
import {By} from '@angular/platform-browser';
import {AppModule} from '../../app.module';
import {SupplierComponent} from './supplier/supplier.component';

describe('SupplierListComponent', () => {
  let component: SuppliersListComponent;
  let fixture: ComponentFixture<SuppliersListComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuppliersListComponent);
    component = fixture.componentInstance;
    component.suppliers = [{
      payment_supplier: 'test_name',
      payment_amount: '4123.342',
      payment_ref: '43124314314',
      payment_cost_rating: '4'
    }, {
      payment_supplier: 'test_name',
      payment_amount: '4123.342',
      payment_ref: '43124314314',
      payment_cost_rating: '4'
    }];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open modal and select Payment', () => {
    component.supplier = {
      payment_cost_rating: '',
      payment_ref: '',
      payment_amount: '',
      payment_supplier: ''
    };
    fixture.detectChanges();
    const supplierComponent = fixture.debugElement.query(By.directive(SupplierComponent));
    supplierComponent.triggerEventHandler('click', null);
    expect(component.supplier).toEqual(component.suppliers[0])
    expect(component.modal).toBeDefined();
    expect(component.modal.open).toEqual(true);
  });
});
