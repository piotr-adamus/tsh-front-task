import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuppliersPaginationComponent } from './suppliers-pagination.component';

describe('SuppliersPaginationComponent', () => {
  let component: SuppliersPaginationComponent;
  let fixture: ComponentFixture<SuppliersPaginationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuppliersPaginationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuppliersPaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
