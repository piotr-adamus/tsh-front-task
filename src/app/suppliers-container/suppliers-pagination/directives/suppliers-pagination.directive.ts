import {AfterContentInit, ContentChildren, Directive, ElementRef, Input, OnDestroy, QueryList, Renderer2} from '@angular/core';
import {Subscription} from 'rxjs';

@Directive({
  selector: '[tshSuppliersPagination]'
})
export class SuppliersPaginationDirective implements AfterContentInit, OnDestroy {
  @Input() leftEnd = false;
  @Input() rightEnd = false;
  @ContentChildren('tshPaginationItem') children: QueryList<ElementRef<HTMLLIElement>>;
  childrenArray: Array<ElementRef<HTMLLIElement>>;
  leftSeperatorNode = this.createPaginationItemSeperator('1');
  rightSeperatorNode = this.createPaginationItemSeperator('2');
  isRightInjected = false;
  isLeftInjected = false;
  childrenSubscription: Subscription;

  constructor(private elRef: ElementRef,
              private renderer: Renderer2) {
  }


  ngAfterContentInit() {
    this.childrenSubscription = this.children.changes
      .subscribe(() => {
        this.injectSeperators();
      });
    this.injectSeperators();
  }

  injectSeperators() {
    this.clearOldNodes();
    this.childrenArray = this.children.toArray();
    if (this.shouldLeftSeperatorBeInjected()) {
      this.renderer.insertBefore(this.elRef.nativeElement, this.leftSeperatorNode, this.childrenArray[1].nativeElement);
      this.isLeftInjected = true;
    }
    if (this.shouldRightSeperatorBeInjected()) {
      this.renderer.insertBefore(this.elRef.nativeElement, this.rightSeperatorNode, this.children.last.nativeElement);
      this.isRightInjected = true;
    }
  }

  shouldLeftSeperatorBeInjected(): boolean {
    return this.leftEnd && +this.childrenArray[1].nativeElement.textContent !== 2;
  }

  shouldRightSeperatorBeInjected(): boolean {
    const arrLength = this.childrenArray.length;
    const isDifferenceMoreThanOne = +this.childrenArray[arrLength - 2].nativeElement.textContent + 1 !==
      +this.childrenArray[arrLength - 1].nativeElement.textContent;
    return this.rightEnd && isDifferenceMoreThanOne;
  }

  createPaginationItemSeperator(idNumber: string): ElementRef<HTMLLIElement> {
    const elem = this.renderer.createElement('li');
    this.renderer.appendChild(elem, this.renderer.createText('...'));
    this.renderer.setAttribute(elem, 'id', idNumber);
    return elem;
  }

  clearOldNodes(): void {
    if (this.isRightInjected) {
      this.renderer.removeChild(this.elRef.nativeElement, this.rightSeperatorNode);
      this.isRightInjected = false;
    }
    if (this.isLeftInjected) {
      this.renderer.removeChild(this.elRef.nativeElement, this.leftSeperatorNode);
      this.isLeftInjected = false;
    }
  }

  ngOnDestroy() {
    this.childrenSubscription.unsubscribe();
  }
}
