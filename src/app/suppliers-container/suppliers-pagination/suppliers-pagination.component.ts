import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {Pagination} from '../shared/models/pagination';

@Component({
  selector: 'tsh-suppliers-pagination',
  templateUrl: './suppliers-pagination.component.html',
  styleUrls: ['./suppliers-pagination.component.scss']
})
export class SuppliersPaginationComponent implements OnChanges {
  @Input() paginationConfig: Pagination = null;
  @Output() onPaginatorClicked = new EventEmitter<string>();
  @Output() leftArrowClicked = new EventEmitter<void>();
  @Output() rightArrowClicked = new EventEmitter<void>();
  linkToLastPage: string;

  constructor() {
  }


  ngOnChanges() {
    if (this.paginationConfig && this.paginationConfig.rightEnd) {
      const paginationLinksLastIndex = +this.paginationConfig.total - 1;
      this.linkToLastPage = this.paginationConfig.links[paginationLinksLastIndex];
    }
  }

  pageChanged(link: string) {
    this.onPaginatorClicked.emit(link);
  }

  onLeftArrowClick(): void {
    this.leftArrowClicked.emit();
  }

  onRightArrowClick(): void {
    this.rightArrowClicked.emit();
  }


}
