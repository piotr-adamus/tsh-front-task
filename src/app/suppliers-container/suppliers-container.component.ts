import {Component, OnInit} from '@angular/core';
import {SupplierRestService} from './supplier-rest-service/supplier-rest.service';
import {SupplierRequest} from './shared/models/supplier-request';
import {Payment} from './shared/models/payment';
import {Pagination} from './shared/models/pagination';

@Component({
  selector: 'tsh-suppliers-container',
  templateUrl: './suppliers-container.component.html',
  styleUrls: ['./suppliers-container.component.scss']
})
export class SuppliersContainerComponent implements OnInit {
  title = 'Where your money goes';
  subtitle = ` Payments made by Chichester District Council to individual suppliers with a value
              over 500 made within October.`;
  error: string = null;

  constructor(private supplierRest: SupplierRestService) {
  }

  suppliers: Array<Payment> = null;
  paginationConfig: Pagination = null;

  ngOnInit() {
    this.getInitialPageData();
  }

  getInitialPageData() {
    this.supplierRest.getSuppliersListPage(0)
      .subscribe((res: SupplierRequest) => {
        this.suppliers = res.payments;
        this.paginationConfig = res.pagination;
      }, (err) => {
        console.log(err);
        this.subtitle = 'Wystąpił błąd w łączeniu z serwerem, odśwież stronę';
      });
  }

  getNextPageData() {
    const nextPageLink = this.paginationConfig.links[+this.paginationConfig.current + 1];
    this.getOtherPageOfSuppliersList(nextPageLink);
  }

  getPreviousPageData() {
    const prevPageLink = this.paginationConfig.links[+this.paginationConfig.current - 1];
    this.getOtherPageOfSuppliersList(prevPageLink);
  }

  getSuppliersWithParams(event: { rating: string, query: string }) {
    this.supplierRest.getSuppliersListPage(0, event.rating, event.query)
      .subscribe((res: SupplierRequest) => {
        this.suppliers = res.payments;
        this.paginationConfig = res.pagination;
      }, () => {
        this.error = 'Wystąpił błąd, spróbuj wprowdzić inne dane wyszukiwania';
        setTimeout(() => {
          this.error = null;
        }, 3000);
      });
  }

  getOtherPageOfSuppliersList(link: string) {
    this.supplierRest.getSuplierListPageFromPagination(link)
      .subscribe((res: SupplierRequest) => {
        this.suppliers = res.payments;
        this.paginationConfig = res.pagination;
      });
  }

}
