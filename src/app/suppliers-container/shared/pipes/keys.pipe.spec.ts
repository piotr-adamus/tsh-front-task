import {KeysPipe} from './keys.pipe';

describe('KeysPipe', () => {
  let pipe;

  beforeEach(() => {
    pipe = new KeysPipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should produce [{"key": "kValue", "val", "valValue"}]', () => {
    const mockObject = {
      'key0': 'val0',
      'key1': 1
    };
    const transformedValue = pipe.transform(mockObject);
    expect(transformedValue[0].key).toBe('key0');
    expect(transformedValue[0].value).toBe('val0');
    expect(transformedValue[1].key).toBe('key1');
    expect(transformedValue[1].value).toBe(1);
  });

  it('should produce [{"key": "kValue", "val", "valValue"}] from Array', () => {
    const mockArray = [
      'val0',
      1
    ];

    const transformedValue = pipe.transform(mockArray);
    expect(transformedValue[0].key).toBe('0');
    expect(transformedValue[0].value).toBe('val0');
    expect(transformedValue[1].key).toBe('1');
    expect(transformedValue[1].value).toBe(1);
  });
});
