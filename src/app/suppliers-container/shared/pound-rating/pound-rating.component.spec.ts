import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoundRatingComponent } from './pound-rating.component';

describe('PoundRatingComponent', () => {
  let component: PoundRatingComponent;
  let fixture: ComponentFixture<PoundRatingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoundRatingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoundRatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
