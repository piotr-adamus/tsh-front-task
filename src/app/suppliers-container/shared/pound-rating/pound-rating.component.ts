import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';

@Component({
  selector: 'tsh-pound-rating',
  templateUrl: './pound-rating.component.html',
  styleUrls: ['./pound-rating.component.scss']
})
export class PoundRatingComponent implements OnChanges {

  @Input() rating: number = null;
  @Input() additionalStyles = null;
  ratingArray: Array<boolean> = [];

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.rating.previousValue !== changes.rating.currentValue) {
      this.fillRatingArray();
    }
  }


  getMobileRating(): string {
    // Wiem że teoretycznie nie ma takiej możliwości ale zabezpieczeń nigdy dość
    return this.rating ? `${this.rating}/5` : 'Ranking nie jest podany';
  }

  fillRatingArray(): void {
    this.ratingArray = [];
    for (let i = 1; i <= 5; i++) {
      this.rating && i <= this.rating ? this.ratingArray.push(true) : this.ratingArray.push(false);
    }
  }

}
