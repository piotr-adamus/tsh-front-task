import {Payment} from './payment';
import {Pagination} from './pagination';

export interface SupplierRequest {
  payments: Array<Payment>;
  pagination: Pagination;
}
