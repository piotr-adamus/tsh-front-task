export interface Payment {
  payment_supplier: string;
  payment_ref: string;
  payment_cost_rating: string;
  payment_amount: string;
}
