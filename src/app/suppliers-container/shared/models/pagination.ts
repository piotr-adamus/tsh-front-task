export interface Pagination {
  total: string;
  current: string;
  links: Array<string>;
  from: string;
  to: string;
  left: boolean;
  right: boolean;
  leftEnd: boolean;
  rightEnd: boolean;
}
