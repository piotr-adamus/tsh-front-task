import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ModalComponent} from './modal.component';

describe('ModalComponent', () => {
  let component: ModalComponent;
  let fixture: ComponentFixture<ModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close the modal-component', () => {
    component.open = true;
    component.closeModal();
    expect(component.open).toBe(false);
  });



  it('shouldn\'t toogle modal-component', () => {
    component.open = false;
    component.closeModal();
    fixture.detectChanges();
    expect(component.open).toBe(false);
  });
});
