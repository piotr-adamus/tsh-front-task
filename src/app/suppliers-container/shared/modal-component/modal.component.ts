import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'tsh-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  open = false;

  constructor() {
  }

  ngOnInit() {

  }

  closeModal(): void {
    this.open = false;
  }
}
