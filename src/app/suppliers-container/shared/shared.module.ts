import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PoundRatingComponent} from './pound-rating/pound-rating.component';
import {KeysPipe} from './pipes/keys.pipe';
import {ModalComponent} from './modal-component/modal.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    PoundRatingComponent,
    KeysPipe,
    ModalComponent
  ],
  exports: [
    PoundRatingComponent,
    KeysPipe,
    ModalComponent
  ]
})
export class SharedModule {
}
