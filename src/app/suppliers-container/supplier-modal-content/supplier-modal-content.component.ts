import {Component, Input, OnInit} from '@angular/core';
import {Payment} from '../shared/models/payment';

@Component({
  selector: 'tsh-supplier-modal-content',
  templateUrl: './supplier-modal-content.component.html',
  styleUrls: ['./supplier-modal-content.component.scss']
})
export class SupplierModalContentComponent implements OnInit {

  @Input() supplier: Payment = null;
  poundRatingStyles = {'paddingLeft': '55px', 'marginTop': '10px'};

  constructor() {
  }

  ngOnInit() {
  }

}
