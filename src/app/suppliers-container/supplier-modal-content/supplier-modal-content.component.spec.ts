import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierModalContentComponent } from './supplier-modal-content.component';

describe('SupplierModalContentComponent', () => {
  let component: SupplierModalContentComponent;
  let fixture: ComponentFixture<SupplierModalContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierModalContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierModalContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
