import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {SupplierRequest} from '../shared/models/supplier-request';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SupplierRestService {

  constructor(private http: HttpClient) {
  }

  getSuppliersListPage(page: string | number = 0, rating?: string | number, query?: string): Observable<SupplierRequest> {
    let reqLink = `${environment.restUrl}?page=${page}`;
    if (rating) {
      reqLink += `&rating=${rating}`;
    }
    if (query) {
      reqLink += `&query=${query}`;
    }

    return this.http.get<SupplierRequest>(reqLink);
  }

  getSuplierListPageFromPagination(link: string) {
    return this.http.get<SupplierRequest>(`${environment.restUrl}?${link}`);
  }

}
